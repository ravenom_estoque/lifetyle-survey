<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lifestyle');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '#Sj`Krm+xp}@DvimD8.WK=|(oKk@s6B=x-J_m)kP<8J&pe4O~!/HS-2nx*sc/79]');
define('SECURE_AUTH_KEY',  'LYysr u>4?aT5]1UG _v_lj@pT~3rJA2961`S%TO>I2NwJvNqh`5gW hTC>Ngkq4');
define('LOGGED_IN_KEY',    'YO#/2J}AQYVd7<&MZVqr}qKU2y>qZvj3N{9ypIl(1u5ro?6vr|vDRgB3rmK) uCj');
define('NONCE_KEY',        'W8pqg5 uj/{R&%n5qt^1,BGB0Rt7qe-VMUnvP4V_fKX2mS}D47r^;. 7bKKtaJkn');
define('AUTH_SALT',        '?z3e<448dzw}FpG-x99.F/awT@|%#p ;+v}8d8^Ol0B+f,@}VOk)=M)[A-KFi[YQ');
define('SECURE_AUTH_SALT', '~Leo6MN:whJ0WF]|k;Zway^(Ms&s(@0ahqqz0+%?4} GbP<JDg)MhEKc:,8%+JF~');
define('LOGGED_IN_SALT',   'jGSV|y@g6p2#X4 (&_U{-Ib_%i{c@YjRzC#Oqa |8=iB5VsbeQ8qiZ-+0Uw*HnGB');
define('NONCE_SALT',       'T) ykK|?Ih}NYq;DnZ{-W!|sM`rI%|z+9+dIfc+TKSpqqSFWFdTHbLMM-fheq8?K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
