			
			</div>

			<!-- /container -->
			<!-- footer -->
			<footer class="footer" role="contentinfo">
				<div class="container">
				<!-- 	<div class="row">
						<ul class="footer-menu">
							<li><a href="index.html">Home</a></li>
							<li><a href="resources-code.html">Code of Ethics</a></li>
							<li><a href="resources-privacy.html">Privacy Policy</a></li>
							<li><a href="resources-terms.html">Terms &amp; Conditions</a></li>
							<li><a href="contact.html">Contact</a></li>
						</ul>
					</div> -->
					<div class="row">
				<!-- copyright -->
					<p class="copyright">
						COPYRIGHT &copy; <?php echo date('Y'); ?> | 
						<a href="http://www.lifestyle-survey.com/" title="lifestyle-survey">WWW.LIFESTYLE-SURVEY.COM</a> 
					</p>
				<!-- /copyright -->
					</div>
				</div>
			</footer>
			<!-- /footer -->


		<?php wp_footer(); ?>

		<!-- analytics -->
		<script>
		(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
		(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
		l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
		ga('send', 'pageview');
		</script>

	</body>
</html>
