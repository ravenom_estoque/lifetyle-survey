<?php
// Script name: register.php
// Description: The page to sign in or to register record in the
?>


<html>
<head>


</HEAD>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/paper/bootstrap.min.css">


<!--[if IE]>
<style type="text/css">
#theform #pt4 {
	padding: 2em 1em 1em 1em;
	}
</style>

<![endif]-->



<script type='text/javascript'>

function formValidator(){
	// Make quick references to our fields
	var fname = document.getElementById('fname');
	var lname = document.getElementById('lname');
	var email = document.getElementById('email');
	var phone = document.getElementById('home');
	var mobile = document.getElementById('mobile');
	var postal = document.getElementById('postal');
	var cont=false;
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(fname, "Please enter only letters for your First Name")==true && isAlphabet(lname, "Please enter only letters for Last Name")==true && emailValidator(email, "Please enter a valid Email Address") ==true && isNumeric(phone, "Please enter a valid UK House Phone Number ") ==true && postit()==true){
		cont=true;
	}
else
{
	cont = false;
	
	}
	
	return cont;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]{11,13}$/i;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z ]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}



function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[A-Z 0-9]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

//Start of Second JS

function postit(){ //check postcode format is valid
 test = document.details.postal.value; size = test.length
 test = test.toUpperCase(); //Change to uppercase
 while (test.slice(0,1) == " ") //Strip leading spaces
  {test = test.substr(1,size-1);size = test.length
  }
 while(test.slice(size-1,size)== " ") //Strip trailing spaces
  {test = test.substr(0,size-1);size = test.length
  }
 document.details.postal.value = test; //write back to form field
 if (size < 6 || size > 8){ //Code length rule
  alert("Please enter a valid Postal Code of UK - Wrong length");
  document.details.postal.focus();
  return false;
  }
 if (!(isNaN(test.charAt(0)))){ //leftmost character must be alpha character rule
   alert("Please enter a valid Postal Code of UK - Cannot start with a number");
   document.details.postal.focus();
   return false;
  }
 if (isNaN(test.charAt(size-3))){ //first character of inward code must be numeric rule
   alert("Please enter a valid Postal Code of UK - alpha character in wrong position");
   document.details.postal.focus();
   return false;
  }
 if (!(isNaN(test.charAt(size-2)))){ //second character of inward code must be alpha rule
   alert("Please enter a valid Postal Code of UK - number in wrong position");
   document.details.postal.focus();
   return false;
  }
 if (!(isNaN(test.charAt(size-1)))){ //third character of inward code must be alpha rule
   alert("Please enter a valid Postal Code of UK - number in wrong position");
   document.details.postal.focus();
   return false;
  }
 if (!(test.charAt(size-4) == " ")){//space in position length-3 rule
   alert("Please enter a valid Postal Code of UK - no space or space in wrong position");
   document.details.postal.focus();
   return false;
   }
 count1 = test.indexOf(" ");count2 = test.lastIndexOf(" ");
 if (count1 != count2){//only one space rule
   alert("Please enter a valid Postal Code of UK - only one space allowed");
   document.details.postal.focus();
   return false;
  }else{

return true;
}
}



</script>




</head>



<!-- (BACKUP NG FORM)

<h2>Register</h2>

<select name = "title">
  <option value="Mr">Mr</option>
  <option value="Mrs">Mrs</option>
  <option value="Ms">Ms</option>
</select> 
<form action="save.php" method="post">
First Name: <input type="text" name="fname"><br>
Last Name: <input type="text" name="lname"><br>
Email Address: <input type="text" name="email"><br>
House Phone Number: <input type="text" name="home"><br>
Mobile Phone Number: <input type="text" name="mobile"><br>
Postal Code: <input type="text" name="postal"><br>
<br><br>
<input type="submit" name="submit" value="Register"><br>
</form>

-->



<body>


<form class="form-horizontal"  id="theform" name="details" action="save.php" onsubmit='return formValidator()' enctype="multipart/form-data" method="post">
	
                    

        <div class="col-sm-4">
      			<fieldset id="pt1">
      				<h5>Step 1:Personal Information</h5>
      				<div class="help">Please provide your real name</div>
      			   <div class="form-group">
      			      <!-- <label for="select" class="col-lg-2 control-label">Selects</label> -->
          			   <div class="col-lg-10">
              			   <select name = "title" class="form-control" id="select">
              					  <option value="Mr">Mr</option>
              					  <option value="Mrs">Mrs</option>
              					  <option value="Ms">Ms</option>
              					</select> 
          				</div>
          		</div>
          		<div class="form-group">
      		                  <!-- <label  class="col-sm-2 control-label" >First&nbsp;Name</label> -->
      		                  <div class="col-lg-10">
      		                       <input type="text" class="form-control" name="fname"  tabindex="1" placeholder="First&nbsp;Name">
      		                  </div>
      		     </div>
      		     <div class="form-group">
      		                 <!--  <label  class="col-sm-2 control-label" >Last&nbsp;Name</label> -->
      		                  <div class="col-lg-10">
      		                       <input type="text" class="form-control" name="lname"  tabindex="2" placeholder="Last&nbsp;Name">
      		                  </div>
      		     </div>     
      			</fieldset>
	       </div>

      	<div class="col-sm-4">
      			<fieldset id="pt2">
          				<h5>Step 2:Contact Information</h5>
          				<div class="help">Required data for us to know you, so we can OptOut you in our database immediately</div><br>
          				
          				<div class="form-group">
          		                  <!-- <label  class="col-sm-2 control-label" >First&nbsp;Name</label> -->
          		                  <div class="col-lg-10">
          		                       <input type="text" class="form-control" name = "email"  tabindex="3" placeholder="Email Address">
          		                  </div>
          		     	</div>

          		     	<div class="form-group">
          		                  <!-- <label  class="col-sm-2 control-label" >First&nbsp;Name</label> -->
          		                  <div class="col-lg-10">
          		                       <input type="text" class="form-control" name="home" tabindex="3" placeholder="Home Phone Number">
          		                  </div>
          		     	</div>
      		        <label for="ex">ex: 0xxxxxxxxxx</label>
      			</fieldset>
       	</div>

  	     <div class="col-sm-4">
            		<fieldset id="pt3">
            			     <h5>Step 3: Postal Code</h5>
            			     <div class="help">Enter your UK Postal Code.</div><br>
            			     <div class="form-group">
            		                  <!-- <label  class="col-sm-2 control-label" >First&nbsp;Name</label> -->
            		                  <div class="col-lg-10">
            		                       <input type="text" class="form-control" name="postal" size="10" maxlength="10"  tabindex="3" placeholder="Postal Code">
            		                  </div>
            		      </div>
            		</fieldset>
	       </div>

 <div class="col-sm-4 col-offset-2">
    	<fieldset id="pt4">
<!--     		<legend>Step 4  : Submit form</legend> -->
    		<h5>Step 4  :Submit form</h5>
        <h6>Terms of Service</h6>
    		<div id="disclaimer">By clicking the &#8220;Submit&#8221; button,
    			You will not receive further communication from us. </div>
    		<input type="submit" id="submit" class="btn btn-primary" name = "submit" tabindex="7" value="Submit &raquo;" onClick="postit()"/>
    	</fieldset>
  </div>



</form>

<!--
<form name="details">
  <b>Postcode: </b>
  <input type="text" name="postal" size="10" maxlength="10">
  <input type="button" name="Button" value="Check Code" onclick="postit()">
</form>
-->



</body>

</html>
