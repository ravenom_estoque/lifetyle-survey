<?php
// Script name: register.php
// Description: The page to sign in or to register record in the
?>


<html>
<head>


</HEAD>


<style type="text/css">
<!--
body {
	background-color: #FFFFFF;
	color: #000000	;
	font-family: Arial, Helvetica, sans-serif;
	}
#theform {
	background-color: #FFFFFF;
	width: 40em;
	}
#theform fieldset {
	height: 20em;
	width: 11em;
	border: 0;
	margin: 0;
	padding: 1em;
	float: left;
	}
#theform fieldset legend {
	font-size: 4em;
	font-family: Georgia, "Times New Roman", Times, serif;
	color: #2791cf;
	}
#theform fieldset legend span {
	display: none;
	}
#theform fieldset h3 {
	height: 4em;
	font-size: 1em;
	}
#theform fieldset div.help {
	color: #f09b16;
	font-size: 0.9em;
	font-weight: bold;
	height: 5em;
	}
#theform fieldset label {
	font-size: 0.7em;
	display: block;
	line-height: 1em;
	}
#theform fieldset input {
	font-size: 0.8em;
	height: 1.5em;
	}
	
/* Error Styling */
#theform fieldset.error,
#theform fieldset.error legend,
#theform fieldset.error div.help {
	color: #FFFFFF;
	}
#theform fieldset strong.error {
	color: #fff;
	background-color: #CC0000;
	padding: 0.2em;
	font-size: 0.7em;
	font-weight: bold;
	display: block;
	}

/* Part 4 Styling */
#theform #pt4 {
	clear: both;
	width: 38em;
	height: 8em;
	border: 10px solid #44b5bc;
	border-width: 2px 0;
	padding: 1em;
	}
#theform #pt4 legend {
	display: none;
	}
#theform #pt4 h3 {
	display: none;
	}
#theform #pt4 #disclaimer {
	width: 22em;
	float: left;
	font-size: 0.7em;
	}
#theform #pt4 input {
	height: 5em;
	font-size: 1em;
	width: 18em;
	color: #FFFFFF; 
	background: #db1838;
	font-weight: bold;
	border-bottom: 	1px solid #999999;
	border-right: 1px solid #999999;
	border-top: 1px solid #CCCCCC;
	border-left: 1px solid #CCCCCC;	
	float: right;
	}

#copyright {
	clear: both;
	padding: 0.5em;
	font-size: 0.8em;
	color: #9F9F00;
	font-style: italic;
	}
-->
</style>

<!--[if IE]>
<style type="text/css">
#theform #pt4 {
	padding: 2em 1em 1em 1em;
	}
</style>

<![endif]-->



<script type='text/javascript'>

function formValidator(){
	// Make quick references to our fields
	var fname = document.getElementById('fname');
	var lname = document.getElementById('lname');
	var email = document.getElementById('email');
	var phone = document.getElementById('home');
	var mobile = document.getElementById('mobile');
	var postal = document.getElementById('postal');
	var cont=false;
	
	// Check each input in the order that it appears in the form!
	if(isAlphabet(fname, "Please enter only letters for your First Name")==true && isAlphabet(lname, "Please enter only letters for Last Name")==true && emailValidator(email, "Please enter a valid Email Address") ==true && isNumeric(phone, "Please enter a valid UK House Phone Number ") ==true && postit()==true){
		cont=true;
	}
else
{
	cont = false;
	
	}
	
	return cont;
	
}

function notEmpty(elem, helperMsg){
	if(elem.value.length == 0){
		alert(helperMsg);
		elem.focus(); // set the focus to this input
		return false;
	}
	return true;
}

function isNumeric(elem, helperMsg){
	var numericExpression = /^[0-9]{11,13}$/i;
	if(elem.value.match(numericExpression)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function isAlphabet(elem, helperMsg){
	var alphaExp = /^[a-zA-Z ]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

function emailValidator(elem, helperMsg){
	var emailExp = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if(elem.value.match(emailExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}



function isAlphanumeric(elem, helperMsg){
	var alphaExp = /^[A-Z 0-9]+$/;
	if(elem.value.match(alphaExp)){
		return true;
	}else{
		alert(helperMsg);
		elem.focus();
		return false;
	}
}

//Start of Second JS

function postit(){ //check postcode format is valid
 test = document.details.postal.value; size = test.length
 test = test.toUpperCase(); //Change to uppercase
 while (test.slice(0,1) == " ") //Strip leading spaces
  {test = test.substr(1,size-1);size = test.length
  }
 while(test.slice(size-1,size)== " ") //Strip trailing spaces
  {test = test.substr(0,size-1);size = test.length
  }
 document.details.postal.value = test; //write back to form field
 if (size < 6 || size > 8){ //Code length rule
  alert("Please enter a valid Postal Code of UK - Wrong length");
  document.details.postal.focus();
  return false;
  }
 if (!(isNaN(test.charAt(0)))){ //leftmost character must be alpha character rule
   alert("Please enter a valid Postal Code of UK - Cannot start with a number");
   document.details.postal.focus();
   return false;
  }
 if (isNaN(test.charAt(size-3))){ //first character of inward code must be numeric rule
   alert("Please enter a valid Postal Code of UK - alpha character in wrong position");
   document.details.postal.focus();
   return false;
  }
 if (!(isNaN(test.charAt(size-2)))){ //second character of inward code must be alpha rule
   alert("Please enter a valid Postal Code of UK - number in wrong position");
   document.details.postal.focus();
   return false;
  }
 if (!(isNaN(test.charAt(size-1)))){ //third character of inward code must be alpha rule
   alert("Please enter a valid Postal Code of UK - number in wrong position");
   document.details.postal.focus();
   return false;
  }
 if (!(test.charAt(size-4) == " ")){//space in position length-3 rule
   alert("Please enter a valid Postal Code of UK - no space or space in wrong position");
   document.details.postal.focus();
   return false;
   }
 count1 = test.indexOf(" ");count2 = test.lastIndexOf(" ");
 if (count1 != count2){//only one space rule
   alert("Please enter a valid Postal Code of UK - only one space allowed");
   document.details.postal.focus();
   return false;
  }else{

return true;
}
}



</script>




</head>



<!-- (BACKUP NG FORM)

<h2>Register</h2>

<select name = "title">
  <option value="Mr">Mr</option>
  <option value="Mrs">Mrs</option>
  <option value="Ms">Ms</option>
</select> 
<form action="save.php" method="post">
First Name: <input type="text" name="fname"><br>
Last Name: <input type="text" name="lname"><br>
Email Address: <input type="text" name="email"><br>
House Phone Number: <input type="text" name="home"><br>
Mobile Phone Number: <input type="text" name="mobile"><br>
Postal Code: <input type="text" name="postal"><br>
<br><br>
<input type="submit" name="submit" value="Register"><br>
</form>

-->



<body>


<center>

	
<form id="theform" name="details" action="save.php" onsubmit='return formValidator()' enctype="multipart/form-data" method="post">
	<fieldset id="pt1">
    
		<legend><span>Step </span>1. <span>: Personal Information</span></legend>
		<h3>Personal Information</h3>
		<div class="help">Please provide your real name</div>
        
        <select name = "title">
  <option value="Mr">Mr</option>
  <option value="Mrs">Mrs</option>
  <option value="Ms">Ms</option>
</select> <br />

		<label for="fname">First Name</label>
		<input type="text" id = "fname" name="fname"tabindex="1" />
        <label for="lname">Last Name</label>
		<input type="text"  id = "lname" name = "lname" tabindex="2" />
        
        
        
        
        
	</fieldset>
	<fieldset id="pt2">
		<legend><span>Step </span>2. <span>:</span></legend>
		<h3>Contact Information</h3>
		<div class="help">Required data for us to know you, so we can OptOut you in our database immediately</div><br>
		<label for="email">Email Address</label>
		<input type="text" id = "email" name = "email"tabindex="3" />
		<label for="home">Home Phone Number</label>
		<input type="text" id = "home" name="home" tabindex="4" /><br>
        <label for="ex">ex: 0xxxxxxxxxx</label>
     
	</fieldset>
  
	<fieldset id="pt3">

		<legend><span>Step </span>3. <span>: Postal Code</span></legend>
		<h3>Postal Code</h3>
		<div class="help">Enter your UK Postal Code.</div><br>
	
		<label for="postal">Postal Code:</label>	

		<!--<input type="text" id = "postal"  name = "postal" tabindex="6" />-->
        
        
        <input type="text" id="postal" name="postal" size="10" maxlength="10">
          <br />
	</fieldset>
  
	<fieldset id="pt4">
		<legend>Step 4  : Submit form</legend>
		<h3>Terms of Service</h3>
		<div id="disclaimer">By clicking the &#8220;Submit&#8221; button,
			You will not receive further communication from us. </div>
		<input type="submit" id="submit" name = "submit" tabindex="7" value="Submit &raquo;" onClick="postit()"/>
	</fieldset>
</form>

<!--
<form name="details">
  <b>Postcode: </b>
  <input type="text" name="postal" size="10" maxlength="10">
  <input type="button" name="Button" value="Check Code" onclick="postit()">
</form>
-->

</center>

</body>

</html>
